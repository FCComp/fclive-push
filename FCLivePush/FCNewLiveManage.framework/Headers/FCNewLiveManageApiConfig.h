//
//  FCNewLiveManageApiConfig.h
//  NewLive
//
//  Created by Summer on 2020/6/19.
//  Copyright © 2020 sobey. All rights reserved.
//

#ifndef FCNewLiveManageApiConfig_h
#define FCNewLiveManageApiConfig_h

// 阿里云上传配置
#define OSS_Upload_Config @"/saas/user/userAccess"

//直播列表
#define Home_Live_List @"/live/v2/live"

//直播详情
#define Live_Detail @"/live/v2/live"


//直播分类
#define Create_Live_Categroy @"/live/v2/category"

//直播发布版面
#define Create_Live_Plate @"/live/v2/plate"

//新建直播
#define Create_Live @"/live/v2/live/save"

//修改直播
#define Create_Update @"/live/v2/live/update"

//删除直播
#define Create_Delete @"/live/v2/live/delete"


//直播流
#define Stream_List @"/live/v2/stream"

//自动获取推流拉流地址
#define Create_Stream_Info @"/live/v2/stream/createSteam"

//新建流
#define Create_Stream @"/live/v2/stream/save"


//删除流
#define Delete_Stream @"/live/v2/stream/delete"

//编辑流
#define Update_Stream @"/live/v2/stream/update"


//直播机位列表
#define CameraStand_List @"/live/v2/live/livestream"

//新建直播机位
#define Create_CameraStand @"/live/v2/live/livestream"

//编辑直播机位
#define Update_CameraStand @"/live/v2/live/livestream"

//删除直播机位
#define Delete_CameraStand @"/live/v2/live/livestream"

//图文直播列表

#define PictureArticle_List @"/live/v2/room/imgtxt"

//新建图文
#define Create_PictureArticle @"/live/v2/room/addimgtxt"

//删除图文
#define Delete_PictureArticle @"/live/v2/room/deleteimgtxt"

//更新图文
#define Update_PictureArticle @"/live/v2/room/updateimgtxt"

//修改图文显示、隐藏状态
#define PictureArticle_Status @"/live/v2/room/changeImagtxtStatus"

//开始直播
#define Begin_Live @"/live/v2/live/onoff"

//更新图文直播的状态
#define Change_Imagtxt_Status @"live/v2/room/changeImagtxtStatus"


//获取是华为还是七牛推流
#define Stream_Config @"/live/v2/stream/getStreamConfig"



//socket地址
#define FCNewLiveManage_Room_Socket @"wss://socketim.sobeylingyun.com/wss"

//socket 接口地址
#define FCNewLiveManage_Room_Socket_Bind_User @""

//socket app_key
#define FCNewLiveManage_Room_Socket_APP_Key @"0DB8853287ED3023F41D4E2C73E23F30"

//socket secret_key
#define FCNewLiveManage_Room_Socket_Secret_Key @"13DDFB1AF7499864EE55B0ED9B9415AE"

//直播间的评论列表
#define FCNewLiveManage_Room_Comment_List @"/live/v2/comment/passcomment"

//直播间添加评论
#define FCNewLiveManage_Room_Add_Comment @"/live/v2/comment/comment"

//请求直播间的token
#define FCNewLiveManage_Room_GetQnRoomToken @"/live/v2/live/getQnRoomToken"

//直播现在用户列表
#define FCNewLiveManage_Room_OnLineUser @"live/api/index/getLineUser"

//直播间的观众信息
#define FCNewLiveManage_Room_Present_UserInfo @"live/api/presents/userInfo"

//直播间的主播信息
#define FCNewLiveManage_Room_Present_LiverInfo @"live/api/presents/liverInfo"

//直播间的个人信息
#define FCNewLiveManage_Room_Present_GetMyInfo @"live/api/presents/getMyInfo"

//直播间的礼物打赏排行榜
#define FCNewLiveManage_Room_Present_SpendSort @"live/api/presents/spendSort"

//直播间的礼物展示列表
#define FCNewLiveManage_Room_Present_List @"live/api/presents/list"

//直播间的礼物打赏
#define FCNewLiveManage_Room_Present_Spend @"live/api/presents/spend"

//直播间的相关配置
#define FCNewLiveManage_Room_Present_Config @"member/presents/config"

//获取直播的状态
#define FCNewLiveManage_Room_Status @"live/api/index/getLiveStatus"

//获取直播的在线观看人数
#define FCNewLiveManage_Room_Watch_Count @"live/api/index/online"

//直播间在线用户列表
#define FCNewLiveManage_Room_OnLineUser @"live/api/index/getLineUser"

//获取直播心跳参数的接口
#define FCNewLiveManage_Room_Heart_Param @"live/api/index/addClick"

//直播心跳
#define FCNewLiveManage_Roome_Heart @"live/api/index/hearts"


#endif /* FCNewLiveManageApiConfig_h */
