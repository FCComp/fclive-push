//
//  FCNewLiveManageDefine.h
//  NewLive
//
//  Created by Summer on 2020/6/22.
//  Copyright © 2020 sobey. All rights reserved.
//

#ifndef FCNewLiveManageDefine_h
#define FCNewLiveManageDefine_h

//直播评论竖屏的宽度
#define kLiveCommentPortraitWidth (FCNewLiveSCREEN_WIDTH - 150)

//直播评论横屏的宽度
#define kLiveCommentLandScapeWidth (FCNewLiveSCREEN_HEIGHT - 150)

//直播底部竖屏的宽度
#define kLivBottomPortraitWidth (FCNewLiveSCREEN_WIDTH - 50)

//直播底部竖屏的高度
#define kLiveBottomPortraitHeight (35)

//直播显示礼物
#define kLiveGiftShowViewPortraitHeight (56 * 2 + 10)



//选择分类、直播流
#define kFCNewLiveManageChooseCateNoti @"FCNewLiveManageChooseCateNoti"


#define kFCNewLiveManageChoosePlateNoti @"FCNewLiveManageChoosePlateNoti"


//新建直播间
#define kFCNewLiveManageCreateLiveNoti @"FCNewLiveManageCreateLiveNoti"

//编辑直播间
#define kFCNewLiveManageUpdateLiveNoti @"FCNewLiveManageUpdateLiveNoti"

//删除直播间
#define kFCNewLiveManageDeleteLiveNoti @"FCNewLiveManageDeleteLiveNoti"

//开始直播
#define kFCNewLiveManageBeginLiveNoti @"FCNewLiveManageBeginLiveNoti"

//结束直播
#define kFCNewLiveManageEndLiveNoti @"FCNewLiveManageEndLiveNoti"

//新建直播流
#define kFCNewLiveManageCreateStreamNoti @"FCNewLiveManageCreateStreamNoti"

//新建直播机位
#define kFCNewLiveManageCreateCameraStandNoti @"FCNewLiveManageCreateCameraStandNoti"


//新建图片直播
#define kFCNewLiveManageCreatePicrureArticleNoti @"FCNewLiveManageCreatePicrureArticleNoti"

//编辑图片直播
#define kFCNewLiveManageUpdatePicrureArticleNoti @"FCNewLiveManageUpdatePicrureArticleNoti"


//直播添加评论
#define kFCNewLiveManageAddCommentNoti @"FCNewLiveManageAddCommentNoti"

//接收到删除评论消息
#define kFCNewLiveManageDeleteCommentNoti @"FCNewLiveManageDeleteCommentNoti"

//主播信息变化
#define kFCNewLiveManageLiveAnchorInfoChangeNoti @"FCNewLiveManageLiveAnchorInfoChangeNoti"

//刷新配置
#define kFCNewLiveManageLivePresentConfigChangeNoti @"FCNewLiveManageLivePresentConfigChangeNoti"

//用户在线宽度改变
#define kFCNewLiveManageLiveOnLineViewWidthChangeNoti @"FCNewLiveManageLiveOnLineViewWidthChangeNoti"

//视频录制的地址
#define kFCNewLiveManageRecordPath @"FCNewLiveManageRecordPath"


//视频录制时间变更
#define kFCNewLiveManageLiveRecordTimeChangeNoti @"FCNewLiveManageLiveRecordTimeChangeNoti"




#endif /* FCNewLiveManageDefine_h */
