//
//  FCLivePush.h
//  FCLivePush
//
//  Created by mac on 2022/2/25.
//

#import <Foundation/Foundation.h>

//! Project version number for FCLivePush.
FOUNDATION_EXPORT double FCLivePushVersionNumber;

//! Project version string for FCLivePush.
FOUNDATION_EXPORT const unsigned char FCLivePushVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <FCLivePush/PublicHeader.h>

#import <FCLivePush/FCLiveShowController.h>

