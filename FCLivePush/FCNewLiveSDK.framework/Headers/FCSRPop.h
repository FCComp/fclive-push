//
//  FCSRPop.h
//  FCSRTools
//
//  Created by Summer on 2020/1/3.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FCNewLiveSDK/FCSSRCommonTools.h>
#import <pop/POP.h>


 typedef void(^POPEndBack)(POPAnimation *anim, BOOL finished);
 typedef POPSpringAnimation*(^POPScaleBack)(POPSpringAnimation *anim);//弹性动画
 typedef POPBasicAnimation*(^POPBasicBack)(POPBasicAnimation *anim);//普通动画
 typedef POPDecayAnimation*(^POPDecayBack)(POPDecayAnimation *anim); //衰减动画
/*
 XIB 动画key 对应 animationType
 */
#define fcsr_Group @"Group"//移动动画集合
#define fcsr_ButtonScale @"ButtonScale"//这个只能button设置 对应可以是bindingAnimationType
#define fcsr_Shake @"Shake"//晃动
#define fcsr_ZoomInX @"ZoomInX"//折叠X
#define fcsr_ZoomInY @"ZoomInY"//折叠Y
#define fcsr_ZoomOut @"ZoomOut"//缩放
#define fcsr_Animations @"animationParams.animations"//动画类型 fcsr_Group专用
#define fcsr_Velocity @"animationParams.velocity"//速率

/**
 fcsr_Bounces 专用
 */
#define fcsr_SpringBounciness   @"animationParams.springBounciness"
#define fcsr_SpringSpeed        @"animationParams.springSpeed"
#define fcsr_DynamicsTension    @"animationParams.dynamicsTension"
#define fcsr_DynamicsFriction   @"animationParams.dynamicsFriction"
#define fcsr_DynamicsMass       @"animationParams.dynamicsMass"


/**
 fcsr_ZoomOut 专用
 */
#define fcsr_Scale      @"animationParams.scale"//缩放比例

/**
 这里可以做animationType key  也可以做 fcsr_Animations 的value
 */
#define fcsr_Right @"BounceRight"
#define fcsr_Left @"BounceLeft"
#define fcsr_Up @"BounceUp"
#define fcsr_Down @"BounceDown"





@interface FCSRPop : NSObject


/*
 动画类型
// CALayer
NSString * const kPOPLayerBackgroundColor = @"backgroundColor";
NSString * const kPOPLayerBounds = @"bounds";
NSString * const kPOPLayerCornerRadius = @"cornerRadius";
NSString * const kPOPLayerBorderWidth = @"borderWidth";
NSString * const kPOPLayerBorderColor = @"borderColor";
NSString * const kPOPLayerOpacity = @"opacity";
NSString * const kPOPLayerPosition = @"position";
NSString * const kPOPLayerPositionX = @"positionX";
NSString * const kPOPLayerPositionY = @"positionY";
NSString * const kPOPLayerRotation = @"rotation";
NSString * const kPOPLayerRotationX = @"rotationX";
NSString * const kPOPLayerRotationY = @"rotationY";
NSString * const kPOPLayerScaleX = @"scaleX";
NSString * const kPOPLayerScaleXY = @"scaleXY";
NSString * const kPOPLayerScaleY = @"scaleY";
NSString * const kPOPLayerSize = @"size";
NSString * const kPOPLayerSubscaleXY = @"subscaleXY";
NSString * const kPOPLayerSubtranslationX = @"subtranslationX";
NSString * const kPOPLayerSubtranslationXY = @"subtranslationXY";
NSString * const kPOPLayerSubtranslationY = @"subtranslationY";
NSString * const kPOPLayerSubtranslationZ = @"subtranslationZ";
NSString * const kPOPLayerTranslationX = @"translationX";
NSString * const kPOPLayerTranslationXY = @"translationXY";
NSString * const kPOPLayerTranslationY = @"translationY";
NSString * const kPOPLayerTranslationZ = @"translationZ";
NSString * const kPOPLayerZPosition = @"zPosition";
NSString * const kPOPLayerShadowColor = @"shadowColor";
NSString * const kPOPLayerShadowOffset = @"shadowOffset";
NSString * const kPOPLayerShadowOpacity = @"shadowOpacity";
NSString * const kPOPLayerShadowRadius = @"shadowRadius";

// CAShapeLayer
NSString * const kPOPShapeLayerStrokeStart = @"shapeLayer.strokeStart";
NSString * const kPOPShapeLayerStrokeEnd = @"shapeLayer.strokeEnd";
NSString * const kPOPShapeLayerStrokeColor = @"shapeLayer.strokeColor";
NSString * const kPOPShapeLayerFillColor = @"shapeLayer.fillColor";
NSString * const kPOPShapeLayerLineWidth = @"shapeLayer.lineWidth";
NSString * const kPOPShapeLayerLineDashPhase = @"shapeLayer.lineDashPhase";

// NSLayoutConstraint
NSString * const kPOPLayoutConstraintConstant = @"layoutConstraint.constant";

#if TARGET_OS_IPHONE

// UIView
NSString * const kPOPViewAlpha = @"view.alpha";
NSString * const kPOPViewBackgroundColor = @"view.backgroundColor";
NSString * const kPOPViewBounds = kPOPLayerBounds;
NSString * const kPOPViewCenter = @"view.center";
NSString * const kPOPViewFrame = @"view.frame";
NSString * const kPOPViewScaleX = @"view.scaleX";
NSString * const kPOPViewScaleXY = @"view.scaleXY";
NSString * const kPOPViewScaleY = @"view.scaleY";
NSString * const kPOPViewSize = kPOPLayerSize;
NSString * const kPOPViewTintColor = @"view.tintColor";

// UIScrollView
NSString * const kPOPScrollViewContentOffset = @"scrollView.contentOffset";
NSString * const kPOPScrollViewContentSize = @"scrollView.contentSize";
NSString * const kPOPScrollViewZoomScale = @"scrollView.zoomScale";
NSString * const kPOPScrollViewContentInset = @"scrollView.contentInset";
NSString * const kPOPScrollViewScrollIndicatorInsets = @"scrollView.scrollIndicatorInsets";

// UITableView
NSString * const kPOPTableViewContentOffset = kPOPScrollViewContentOffset;
NSString * const kPOPTableViewContentSize = kPOPScrollViewContentSize;

// UICollectionView
NSString * const kPOPCollectionViewContentOffset = kPOPScrollViewContentOffset;
NSString * const kPOPCollectionViewContentSize = kPOPScrollViewContentSize;

// UINavigationBar
NSString * const kPOPNavigationBarBarTintColor = @"navigationBar.barTintColor";

// UIToolbar
NSString * const kPOPToolbarBarTintColor = kPOPNavigationBarBarTintColor;

// UITabBar
NSString * const kPOPTabBarBarTintColor = kPOPNavigationBarBarTintColor;

// UILabel
NSString * const kPOPLabelTextColor = @"label.textColor";

#else

// NSView
NSString * const kPOPViewFrame = @"view.frame";
NSString * const kPOPViewBounds = @"view.bounds";
NSString * const kPOPViewAlphaValue = @"view.alphaValue";
NSString * const kPOPViewFrameRotation = @"view.frameRotation";
NSString * const kPOPViewFrameCenterRotation = @"view.frameCenterRotation";
NSString * const kPOPViewBoundsRotation = @"view.boundsRotation";

// NSWindow
NSString * const kPOPWindowFrame = @"window.frame";
NSString * const kPOPWindowAlphaValue = @"window.alphaValue";
NSString * const kPOPWindowBackgroundColor = @"window.backgroundColor";

#endif

#if SCENEKIT_SDK_AVAILABLE

// SceneKit
NSString * const kPOPSCNNodePosition = @"scnode.position";
NSString * const kPOPSCNNodePositionX = @"scnnode.position.x";
NSString * const kPOPSCNNodePositionY = @"scnnode.position.y";
NSString * const kPOPSCNNodePositionZ = @"scnnode.position.z";
NSString * const kPOPSCNNodeTranslation = @"scnnode.translation";
NSString * const kPOPSCNNodeTranslationX = @"scnnode.translation.x";
NSString * const kPOPSCNNodeTranslationY = @"scnnode.translation.y";
NSString * const kPOPSCNNodeTranslationZ = @"scnnode.translation.z";
NSString * const kPOPSCNNodeRotation = @"scnnode.rotation";
NSString * const kPOPSCNNodeRotationX = @"scnnode.rotation.x";
NSString * const kPOPSCNNodeRotationY = @"scnnode.rotation.y";
NSString * const kPOPSCNNodeRotationZ = @"scnnode.rotation.z";
NSString * const kPOPSCNNodeRotationW = @"scnnode.rotation.w";
NSString * const kPOPSCNNodeEulerAngles = @"scnnode.eulerAngles";
NSString * const kPOPSCNNodeEulerAnglesX = @"scnnode.eulerAngles.x";
NSString * const kPOPSCNNodeEulerAnglesY = @"scnnode.eulerAngles.y";
NSString * const kPOPSCNNodeEulerAnglesZ = @"scnnode.eulerAngles.z";
NSString * const kPOPSCNNodeOrientation = @"scnnode.orientation";
NSString * const kPOPSCNNodeOrientationX = @"scnnode.orientation.x";
NSString * const kPOPSCNNodeOrientationY = @"scnnode.orientation.y";
NSString * const kPOPSCNNodeOrientationZ = @"scnnode.orientation.z";
NSString * const kPOPSCNNodeOrientationW = @"scnnode.orientation.w";
NSString * const kPOPSCNNodeScale = @"scnnode.scale";
NSString * const kPOPSCNNodeScaleX = @"scnnode.scale.x";
NSString * const kPOPSCNNodeScaleY = @"scnnode.scale.y";
NSString * const kPOPSCNNodeScaleZ = @"scnnode.scale.z";
NSString * const kPOPSCNNodeScaleXY = @"scnnode.scale.xy";
*/


/**
 弹性动画

 @param name 动画类型
 @param toValue 动画结束状态   必须传
 @param fromValue 动画开始状态  可以传nil
 @param view 做动画的View的    必须传
 @param popScaleBack 如果有额外参数，请在这里面写  可以传nil
 @param popBlock 回调 可以传nil
 */
+(void)fcsr_ScaleAnimationWithNamed:(NSString *)name toValue:(id)toValue fromValue:(id)fromValue view:(id)view popScaleBack:(POPScaleBack)popScaleBack  popBlock:(POPEndBack)popBlock;


/**
 普通动画
 
 @param name 动画类型
 @param toValue 动画结束状态   必须传
 @param fromValue 动画开始状态  可以传nil
 @param view 做动画的View的    必须传
 @param popBasicBack 如果有额外参数，请在这里面写  可以传nil
 @param popBlock 回调 可以传nil
 */
+(void)fcsr_BasicAnimationWithNamed:(NSString *)name toValue:(id)toValue fromValue:(id)fromValue view:(id)view popBasicBack:(POPBasicBack)popBasicBack  popBlock:(POPEndBack)popBlock;


/**
 衰减动画

 @param name 动画类型
 @param fromValue 动画开始状态
 @param velocity 速率 必须传
 @param view 做动画的View的    必须传
 @param popDecayBack 如果有额外参数，请在这里面写  可以传nil
 @param popBlock 回调 可以传nil
 */
+(void)fcsr_DecayAnimationWithNamed:(NSString *)name  fromValue:(id)fromValue velocity:(id)velocity view:(id)view popDecayBack:(POPDecayBack)popDecayBack  popBlock:(POPEndBack)popBlock;






/**
 获取一个弹性动画对象 POPSpringAnimation

 @param name 动画类型
 @param toValue 动画结束状态   必须传
 @param fromValue 动画开始状态  可以传nil
 @param popScaleBack 如果有额外参数，请在这里面写  可以传nil
 @return POPSpringAnimation
 */
+(POPSpringAnimation*)fcsr_getScaleAnimationWithNamed:(NSString *)name toValue:(id)toValue fromValue:(id)fromValue popScaleBack:(POPScaleBack)popScaleBack  popBlock:(POPEndBack)popBlock;
;
/**
获取一个普通动画对象 POPBasicAnimation
 
 @param name 动画类型
 @param toValue 动画结束状态   必须传
 @param fromValue 动画开始状态  可以传nil
 @param popBasicBack 如果有额外参数，请在这里面写  可以传nil
 @param popBlock 回调 可以传nil
 */
+(POPBasicAnimation*)fcsr_getBasicAnimationWithNamed:(NSString *)name toValue:(id)toValue fromValue:(id)fromValue  popBasicBack:(POPBasicBack)popBasicBack  popBlock:(POPEndBack)popBlock;
/**
 获取一个衰减动画对象 POPDecayAnimation
 
 @param name 动画类型
 @param fromValue 动画开始状态
 @param velocity 速率 必须传
 @param popDecayBack 如果有额外参数，请在这里面写  可以传nil
 @param popBlock 回调 可以传nil
 */
+(POPDecayAnimation*)fcsr_getDecayAnimationWithNamed:(NSString *)name  fromValue:(id)fromValue velocity:(id)velocity  popDecayBack:(POPDecayBack)popDecayBack  popBlock:(POPEndBack)popBlock;
/**
 移除所有动画
 */
+(void)fcsr_removeAllAnimations:(id)view;


/**
 添加一个push动画

 @param subtype 动画方向 kCATransitionFromLeft kCATransitionFromRight kCATransitionFromTop kCATransitionFromBottom
 @param layer 图层
 @param duration 时间
 */
+(void)fcsr_pushAnimationWithSubtype:(NSString *)subtype  layer:(CALayer*)layer duration:(double)duration;


@end


