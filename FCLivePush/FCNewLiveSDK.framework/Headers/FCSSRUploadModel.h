//
//  FCSSRUploadModel.h
//  FCSRTools
//
//  Created by Summer on 2020/3/12.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Photos/Photos.h>

NS_ASSUME_NONNULL_BEGIN

@interface FCSSRUploadModel : NSObject
/**
   type = 1图片，
   type = 2视频
 */
@property (nonatomic , assign) NSInteger type;
@property (nonatomic , strong) NSData  *data;//上传的data
@property (nonatomic , strong) PHAsset *imagePHAsset;//图片相册资源
@property (nonatomic , strong) PHAsset *videoPHAsset;//视频相册资源
@property (nonatomic , copy) NSString *source_url;//编辑时的资源地址
@property (nonatomic , strong) UIImage *videoImage;//视频的第一帧图片
@end

NS_ASSUME_NONNULL_END
