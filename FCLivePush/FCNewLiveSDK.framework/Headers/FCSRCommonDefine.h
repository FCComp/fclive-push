//
//  FCSRCommonDefine.h
//  FCNewLiveSDK
//
//  Created by Summer on 2019/12/26.
//  Copyright © 2019 Summer. All rights reserved.
//

#ifndef FCSRCommonDefine_h
#define FCSRCommonDefine_h

//颜色
#define FCSRColorFromHX(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define FCNewLiveColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define FCNewLiveCOLOR_RGB_ALPHA(rgbValue,i) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:(i)]

#define kKeyWindow [[[UIApplication sharedApplication]delegate]window]

#define kServiceError @"糟糕,服务器竟然崩溃啦~"
#define kNetworkError @"似乎已断开与互联网的连接~"

#define kFCSR_IMGURL(URL) [FCSSRCommonTools getImgUrl:URL]
#define kFCSR_IMG(name) [UIImage imageNamed:name]

#define kFCSR_PingFangBold(size1) [UIFont fontWithName:@"PingFangSC-Semibold" size:size1]

#define kFCSR_PingFangMedium(size1) [UIFont fontWithName:@"PingFangSC-Medium" size:size1]

#define kFCSR_PingFangRegular(size1) [UIFont fontWithName:@"PingFangSC-Regular" size:size1]

#define kFCSR_LineColor FCSRColorFromHX(0xF4F4F4)

#define KISIphoneX \
({BOOL isPhoneX = NO;\
if (@available(iOS 11.0, *)) {\
isPhoneX = [[UIApplication sharedApplication] delegate].window.safeAreaInsets.bottom > 0.0;\
}\
(isPhoneX);})

#define kStatusBarHeight (KISIphoneX?(44.f):(20.f))

#define kBottomSafeHeight (KISIphoneX?(34.f):(0.f))

#define kWidthScale (FCNewLiveSCREEN_WIDTH / 375.0)

#define kISIphone5 (F_EQUAL(FCNewLiveSCREEN_WIDTH, 320))

#define kWidthAdaption(width)   (F_EQUAL(FCNewLiveSCREEN_WIDTH, 320)?(width * kWidthScale):(width))

#define FCNewLiveSCREEN_RECT     [UIScreen mainScreen].bounds
#define FCNewLiveSCREEN_WIDTH     [UIScreen mainScreen].bounds.size.width
#define FCNewLiveSCREEN_HEIGHT   [UIScreen mainScreen].bounds.size.height

#define FCNewLiveBundle(bundle)    [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:(bundle) ofType:@"bundle"]]

#define FCNewLiveFORMAT(fmt,...)[NSString stringWithFormat:fmt,##__VA_ARGS__]


//weak self
#define FCNewLiveWS(weakSelf)  __weak __typeof(&*self)weakSelf = self

/**
 合成弱引用/强引用
 
 Example:
     @weakify(self)
     [self doSomething^{
         @strongify(self)
         if (!self) return;
         ...
     }];
 
 */
#ifndef weakify
    #if DEBUG
        #if __has_feature(objc_arc)
            #define weakify(object) autoreleasepool{} __weak __typeof__(object) weak##_##object = object;
        #else
            #define weakify(object) autoreleasepool{} __block __typeof__(object) block##_##object = object;
        #endif
    #else
        #if __has_feature(objc_arc)
            #define weakify(object) try{} @finally{} {} __weak __typeof__(object) weak##_##object = object;
        #else
            #define weakify(object) try{} @finally{} {} __block __typeof__(object) block##_##object = object;
        #endif
    #endif
#endif

#ifndef strongify
    #if DEBUG
        #if __has_feature(objc_arc)
            #define strongify(object) autoreleasepool{} __typeof__(object) object = weak##_##object;
        #else
            #define strongify(object) autoreleasepool{} __typeof__(object) object = block##_##object;
        #endif
    #else
        #if __has_feature(objc_arc)
            #define strongify(object) try{} @finally{} __typeof__(object) object = weak##_##object;
        #else
            #define strongify(object) try{} @finally{} __typeof__(object) object = block##_##object;
        #endif
    #endif
#endif


#ifndef dispatch_queue_async_safe
#define dispatch_queue_async_safe(queue, block)\
if (strcmp(dispatch_queue_get_label(DISPATCH_CURRENT_QUEUE_LABEL), dispatch_queue_get_label(queue)) == 0) {\
block();\
} else {\
dispatch_async(queue, block);\
}
#endif


/** 设置FCSRLog可以打印出类名,方法名,行数 */
#ifdef DEBUG

#define FCSRLog(...) {}

#else
#define FCSRLog(...) {}
#endif

#endif /* FCSRCommonDefine_h */
