//
//  FCSRTextView.h
//  FCSRTools
//
//  Created by Summer on 2020/2/27.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FCSRTextView : UIView

@property (nonatomic,strong) UITextView *textView;
@property (nonatomic,strong) UILabel *placeHolderLabel;//占位符
@property (nonatomic,strong) UILabel *wordCountLabel;//计算字数

@property (nonatomic,copy) NSString *placeHolder;
@property (nonatomic,assign) NSInteger limitLength;//最大字数

@property (nonatomic,assign) NSInteger sole_tag;

@property (nonatomic, copy) NSString *inputText;

@property (nonatomic, copy) void (^textBlock)(NSInteger sole_tag,NSString *text);

@end

NS_ASSUME_NONNULL_END
