//
//  FCNewLiveSDKMessageDefine.h
//  FCNewLiveProject
//
//  Created by Summer on 2019/12/26.
//  Copyright © 2019 Summer. All rights reserved.
//

#ifndef FCNewLiveSDKMessageDefine_h
#define FCNewLiveSDKMessageDefine_h

//用户点击推送通知后
#define FCNewLiveMessageNotification_ReceivedNotification      @"FCNewLiveMessageNotification_ReceivedNotification"

//启动加载完成，启动页消失后由导航组件出发
#define FCNewLiveMessageNotification_DidFinishLoading          @"FCNewLiveMessageNotification_DidFinishLoading"

//注销登录,注销时发送此通知
#define FCNewLiveMessageNotification_DidLogout                 @"FCNewLiveMessageNotification_DidLogout"

#define FCNewLiveMessageNotification_DidFinishLaunching        @"FCNewLiveMessageNotification_DidFinishLaunching"

#define FCNewLiveMessageNotification_WillResignActive          @"FCNewLiveMessageNotification_WillResignActive"

#define FCNewLiveMessageNotification_DidEnterBackground        @"FCNewLiveMessageNotification_DidEnterBackground"

#define FCNewLiveMessageNotification_WillEnterForeground       @"FCNewLiveMessageNotification_WillEnterForeground"

#define FCNewLiveMessageNotification_DidBecomeActive           @"FCNewLiveMessageNotification_DidBecomeActive"

#define FCNewLiveMessageNotification_WillTerminate             @"FCNewLiveMessageNotification_WillTerminate"


//用户token过期
#define FCNewLiveMessageNotification_TokenInValide  @"FCNewLiveMessageNotification_TokenInValide"
//接收到openURL消息
#define FCNewLiveAppDelegateHandleOpenURLNotification             @"FCNewLiveAppDelegateHandleOpenURLNotification"
#define FCNewLiveAppDelegateHandleOpenURLAndOpenUrlOptionsNotification            @"FCNewLiveAppDelegateHandleOpenURLAndOpenUrlOptionsNotification"

#define FCNewLiveAppDelegateHandleContinueUserActivityNotification                @"FCNewLiveAppDelegateHandleContinueUserActivityNotification"

#define FCNewLiveAppDelegateHandleContinueUserActivityNotification                @"FCNewLiveAppDelegateHandleContinueUserActivityNotification"

#endif /* FCNewLiveSDKMessageDefine_h */
