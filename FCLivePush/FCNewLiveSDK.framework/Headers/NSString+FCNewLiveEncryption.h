//
//  NSString+FCNewLiveEncryption.h
//  FCNewLiveSDK
//
//  Created by ZhouYou on 2019/12/26.
//  Copyright © 2019 ZhouYou. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (FCNewLiveEncryption)

/**
 md5加密

 @return 加密后的字符串
 */
- (NSString *)md5;

/**
 *  转换为Base64编码
 */
- (NSString *)base64EncodedString;
/**
 *  将Base64编码还原
 */
- (NSString *)base64DecodedString;

/**
 生成32位随机字符串

 @return 返回生成的随机字符串
 */
+ (NSString *)generateUUID;

/**
 字典转json字符串

 @param dict 待转换字典
 @return json字符串
 */
+ (NSString *)convertToJsonData:(NSDictionary *)dict;

/**
 json字符串转换字典

 @param jsonString 待转换字符串
 @return 字典
 */
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;

@end

NS_ASSUME_NONNULL_END
