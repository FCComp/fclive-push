//
//  FCSRRequestCancelManager.h
//  FCSRTools
//
//  Created by Summer on 2020/1/3.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <FCNewLiveSDK/FCSSRCommonTools.h>

NS_ASSUME_NONNULL_BEGIN

@interface FCSRRequestCancelModel : NSObject
@property (nonatomic, strong) NSString *vc;
@property (nonatomic, strong) NSURLSessionDataTask *task;

@end


@interface FCSRRequestCancelManager : NSObject

@property (nonatomic, strong) NSMutableArray *requestModels;

+(instancetype)shareManager;


/**
 添加task
 @param task 当前task
 */
- (void)addTask:(NSURLSessionDataTask *)task;

/**
 移除task

 @param task 当前task
 */
- (void)removeTask:(NSURLSessionDataTask *)task;

/**
 取消网络请求

 @param vc 当前vc
 */
- (void)cancelAllRequestWithVc:(NSString *)vc;

@end

NS_ASSUME_NONNULL_END
