//
//  FCNewLiveTenantUser.h
//  FCNewLiveSDK
//
//  Created by sobey on 2020/8/25.
//  Copyright © 2020 sobey. All rights reserved.
// 租户信息

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FCNewLiveTenantUser : NSObject<NSCoding,NSCopying>

@property (nonatomic , assign) NSInteger user_id;//用户ID
@property (nonatomic , copy) NSString *user_name;//用户名字
@property (nonatomic , copy) NSString *user_portrait;//用户头像
@property (nonatomic , copy) NSString *user_token;//用户token
@property (nonatomic , copy) NSString *user_mobile;//用户手机号
@property (nonatomic , assign) NSInteger tenant_id;//租户ID
@property (nonatomic , copy) NSString *tenant_name;//租户名字
@property (nonatomic , copy) NSString *tenant_access_key;//租户access_key


@end

NS_ASSUME_NONNULL_END
