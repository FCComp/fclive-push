//
//  FCNewLiveViewController.h
//  FCNewLiveSDK
//
//  Created by Summer on 2019/12/26.
//  Copyright © 2019 Summer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FCNewLiveSDK/FCSRBaseVM.h>

NS_ASSUME_NONNULL_BEGIN

@interface FCNewLiveViewController : UIViewController

@property (nonatomic , strong) FCSRBaseVM *basevm;

/**
  isEmptyLoad = YES 显示加载视图
  isEmptyLoad = NO 不显示加载视图
 */
@property (nonatomic,assign) BOOL isEmptyLoad;
//empty
@property (nonatomic,assign) BOOL isShowEmptyView;
@property (nonatomic, copy) NSString *nodataStr;
@property (nonatomic, copy) NSString *nodataImgStr;

/**
 添加 tableView 下拉刷新
 */
- (void)fcsr_addRefreshHeader;
/**
 添加tableView 上拉加载
 */
- (void)fcsr_addRefreshFooter;
/**
 绑定 tableView
 */
- (void)fcsr_bindTableView;


/**
 绑定 collectionView
 */
- (void)fcsr_bindCollectionView;
/**
 添加 collectionView 下拉刷新
 */
- (void)fcsr_addCollectionViewRefreshHeader;
/**
 添加 collectionView 上拉加载
 */
- (void)fcsr_addCollectionViewRefreshFooter;

- (void)fcsr_endRefreshing;

- (void)fcsr_endRefreshingWithList:(NSArray *)list;
 
- (UIButton *)fcsr_getlLeftBackBtn;


- (void)fcsr_addLeftBackBtn;

- (void)onBackClick:(UIButton*)button;

/**
 是否允许横屏，默认为不允许横屏
 */
@property (nonatomic, assign) BOOL allowLandscape;
/**
 * 用于对Controller的参数传递，标准json格式等字符串
 */
@property (nonatomic, copy) NSString *paramStr;

/**
 请求数据
 */
- (void)fcsr_requestData;

/**
 * 对一个Native的Controller发送消息
 * message  消息内容，标准Json格式
 * controller  目标Controller
 * from 发送者，自由定义
 */
- (void)sendFCNewLiveMessage:(NSString *)message toController:(id)controller from:(NSString *)from;

/**
 * 对一个Native的Controller发送消息
 * message  消息内容，标准Json格式
 * className  目标Controller类名
 * from 发送者，自由定义
 */
- (void)sendFCNewLiveMessage:(NSString *)message toControllerClassName:(NSString *)className from:(NSString *)from;

/**
 * 收到消息的回调，需重写此方法
 * message  消息内容，标准json格式
 */
- (void)receivedFCNewLiveMessage:(NSDictionary *)message;

/**
 * 根据ParamStr自动转换成字典（若paramStr为标准json格式）
 */
- (NSDictionary *)paramDict;



@end

NS_ASSUME_NONNULL_END
