//
//  FCSRDashCornerButton.h
//  FCSRTools
//
//  Created by Summer on 2020/3/3.
//  Copyright © 2020 sobey. All rights reserved.
// 虚线圆角button

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FCSRDashCornerButton : UIButton
@property (nonatomic,strong) UIColor *dashLineColor;//虚线颜色
@property (nonatomic,assign) CGFloat dashLineWidth;//虚线边框宽度
@property (nonatomic,assign) CGFloat dashCornerRadius;//圆角

@property (nonatomic,assign) CGFloat width1;//实线宽度
@property (nonatomic,assign) CGFloat width2;//虚线宽度
@end

NS_ASSUME_NONNULL_END
