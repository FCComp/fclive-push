//
//  FCNewLiveBaseHttpSSLConfig.h
//  FCNewLiveSDK
//
//  Created by Summer on 2020/1/13.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

//与AFSSLPinningMode对应，使用时可以直接使用，也可以强转
typedef NS_ENUM(NSUInteger, FCNewLiveSSLPinningMode) {
    //与AFSSLPinningModeNone对应
    FCNewLiveSSLPinningModeNone,
    //与FCNewLiveSSLPinningModeCertificate对应
    FCNewLiveSSLPinningModeCertificate,
};

@interface FCNewLiveBaseHttpSSLConfig : NSObject

/**
 https证书类型,只有AFSSLPinningModeNone和AFSSLPinningModeCertificate两种类型
 */
@property (nonatomic, assign) FCNewLiveSSLPinningMode pinningMode;

/**
 https证书数据
 */
@property (nonatomic, strong) id certificateData;

/**
 https证书密码
 */
@property (nonatomic, strong) NSString *certificatePassword;

/**
 是否允许无效证书
 */
@property (nonatomic, assign) BOOL allowInvalidCertificates;

/**
 是否需要验证域名
 */
@property (nonatomic, assign) BOOL validatesDomainName;

@end

NS_ASSUME_NONNULL_END
