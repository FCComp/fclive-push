//
//  FCSRToastManager.h
//  FCSRTools
//
//  Created by Summer on 2020/1/8.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MBProgressHUD/MBProgressHUD.h>

NS_ASSUME_NONNULL_BEGIN

@interface FCSRToastManager : NSObject

+ (MBProgressHUD *)showLoading:(UIView *)view;

+ (MBProgressHUD *)showLoading:(UIView *)view text:(NSString *)text;

+ (void)showText:(NSString *)text toView:(UIView *)view time:(CGFloat)time;

+ (void)showText:(NSString *)text toView:(UIView *)view;

+ (void)showText:(NSString *)text;

+ (void)showSuccess:(NSString *)text toView:(UIView *)view time:(CGFloat)time;

+ (void)showSuccess:(NSString *)text toView:(UIView *)view;

+ (void)showSuccess:(NSString *)text;

+ (void)showFial:(NSString *)text toView:(UIView *)view time:(CGFloat)time;

+ (void)showFial:(NSString *)text toView:(UIView *)view;

+ (void)showFial:(NSString *)text;

+ (void)showWarning:(NSString *)text toView:(UIView *)view time:(CGFloat)time;

+ (void)showWarning:(NSString *)text toView:(UIView *)view;

+ (void)showWarning:(NSString *)text;

@end

NS_ASSUME_NONNULL_END
