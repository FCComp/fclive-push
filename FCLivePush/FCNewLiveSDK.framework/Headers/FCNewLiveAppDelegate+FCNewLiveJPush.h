//
//  FCNewLiveAppDelegate+FCNewLiveJPush.h
//  FCNewLiveSDK
//
//  Created by Summer on 2019/12/27.
//  Copyright © 2019 Summer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FCNewLiveAppDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface FCNewLiveAppDelegate (FCNewLiveJPush)

@property (nonatomic, strong, nullable) NSDictionary *launchUserInfo;

- (void)FCNewLiveJPushApplication:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions;

- (void)FCNewLiveApplicationWillResignActive:(UIApplication *)application;

- (void)FCNewLiveApplicationDidEnterBackground:(UIApplication *)application;

- (void)FCNewLiveApplicationWillEnterForeground:(UIApplication *)application;

- (void)FCNewLiveApplicationDidBecomeActive:(UIApplication *)application;

- (void)FCNewLiveApplicationWillTerminate:(UIApplication *)application;

- (void)FCNewLiveApplication:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;

- (void)FCNewLiveApplication:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error;

- (void)FCNewLiveApplication:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler;

@end

NS_ASSUME_NONNULL_END
