//
//  FCSSRNetworkErrorView.h
//  FCSRTools
//
//  Created by Summer on 2020/5/14.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^FCSRNetworkErrorAction)(void);

@interface FCSSRNetworkErrorView : UIView

- (instancetype)initWithTips:(NSString *)tips refreshTips:(NSString *)refreshTips;
- (void)setBgImage:(UIImage *)image;
- (void)setbgImageSize:(CGSize)size;
- (void)setTip:(NSString *)tip;
- (void)setTipTextColor:(UIColor *)textColor;
- (void)setRefreshTip:(NSString *)refreshTip;
- (void)setRefreshTipTextColor:(UIColor *)textColor;
- (void)setupTapAction:(FCSRNetworkErrorAction)action;
@end

NS_ASSUME_NONNULL_END
