//
//  UIButton+FCSSRImageTitleSpacing.h
//  FCSRTools
//
//  Created by Summer on 2020/3/16.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

// 定义一个枚举（包含了四种类型的button）
typedef NS_ENUM(NSUInteger, FCSRButtonEdgeInsetsStyle) {
    FCSRButtonEdgeInsetsStyleTop, // image在上，label在下
    FCSRButtonEdgeInsetsStyleLeft, // image在左，label在右
    FCSRButtonEdgeInsetsStyleBottom, // image在下，label在上
    FCSRButtonEdgeInsetsStyleRight // image在右，label在左
};


@interface UIButton (FCSSRImageTitleSpacing)

/**
 *  设置button的titleLabel和imageView的布局样式，及间距
 *
 *  @param style titleLabel和imageView的布局样式
 *  @param space titleLabel和imageView的间距
 */
- (void)fcsr_layoutButtonWithEdgeInsetsStyle:(FCSRButtonEdgeInsetsStyle)style
                        imageTitleSpace:(CGFloat)space;

@end

NS_ASSUME_NONNULL_END
