//
//  FCSSRHudLoadingView.h
//  FCSRTools
//
//  Created by Summer on 2020/5/14.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MBProgressHUD/MBProgressHUD.h>

NS_ASSUME_NONNULL_BEGIN

@interface FCSSRHudLoadingView : UIView

@property (nonatomic) NSString *tips;
@property(nonatomic, strong) MBProgressHUD *hud;

@end

NS_ASSUME_NONNULL_END
