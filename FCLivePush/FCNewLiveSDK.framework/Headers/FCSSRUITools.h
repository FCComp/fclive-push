//
//  FCSSRUITools.h
//  FCNewLiveSDK
//
//  Created by Summer on 2020/6/18.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FCSSRUITools : NSObject

//label

+ (UILabel *)labelWithText:(NSString *)text textColor:(UIColor *)textColor font:(UIFont *)font;

+ (UILabel *)labelWithText:(NSString *)text textColor:(UIColor *)textColor font:(UIFont *)font  textAlignment:(NSTextAlignment)textAlignment;

//filed

+ (UITextField *)filedWithPlaceholder:(NSString *)placeholder  textColor:(UIColor *)textColor font:(UIFont *)font;

+ (UITextField *)filedWithPlaceholder:(NSString *)placeholder placeholderColor:(UIColor *)placeholderColor textColor:(UIColor *)textColor font:(UIFont *)font keyboardType:(UIKeyboardType)keyboardType clearButtonMode:(UITextFieldViewMode)clearButtonMode;

//button

+ (UIButton *)btnWithTitle:(NSString *)title titleColor:(UIColor *)titleColor  font:(UIFont *)font;

    
+ (UIButton *)btnWithTitle:(NSString *)title titleColor:(UIColor *)titleColor selectedColor:(UIColor *)selectedColor backgroundColor:(UIColor *)backgroundColor font:(UIFont *)font  cornerRadius:(CGFloat)cornerRadius;

+ (UIButton *)btnWithImage:(NSString *)image selectedImage:(NSString *)selectedImage;

+ (UIButton *)btnWithImage:(NSString *)image title:(NSString *)title titleColor:(UIColor *)titleColor  font:(UIFont *)font;

//imageView
+ (UIImageView *)imageVWithImage:(NSString *)image cornerRadius:(CGFloat)cornerRadius;

@end

NS_ASSUME_NONNULL_END
