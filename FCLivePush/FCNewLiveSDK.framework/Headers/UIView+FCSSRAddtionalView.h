//
//  UIView+FCSSRAddtionalView.h
//  FCSRTools
//
//  Created by Summer on 2020/5/14.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FCSSRActivityLoadingView.h"
#import "FCSSRHudLoadingView.h"
#import "FCSSRDataEmptyView.h"
#import "FCSSRNetworkErrorView.h"

NS_ASSUME_NONNULL_BEGIN

@interface UIView (FCSSRAddtionalView)

@property (nullable, nonatomic) FCSSRActivityLoadingView *activityView;
@property (nullable, nonatomic) FCSSRHudLoadingView *hudView;
@property (nullable, nonatomic) FCSSRDataEmptyView *emptyView;
@property (nullable, nonatomic) FCSSRNetworkErrorView *errorView;
@property (nullable, nonatomic) void (^goBackAction)(void);

//activity
- (void)fcsr_addActivityView;
- (void)fcsr_addActivityViewWithTips:(nullable NSString *)tips;
- (void)fcsr_removeActivityView;

//hud
- (void)fcsr_addHudView;
- (void)fcsr_addHudViewWithTips:(nullable NSString *)tips;
- (void)fcsr_removeHudView;

//empty

- (void)fcsr_addEmptyView;
- (void)fcsr_addEmptyViewWithTapAction:(nullable FCSRDataEmptyAction)action;
- (void)fcsr_addEmptyViewWithImage:(nullable UIImage *)image tips:(nullable NSString *)tips;

- (void)fcsr_addEmptyViewWithImage:(nullable UIImage *)image
                              tips:(nullable NSString *)tips
                         tapAction:(nullable FCSRDataEmptyAction)action;

- (void)fcsr_addEmptyViewWithImage:(nullable UIImage *)image
                              tips:(nullable NSString *)tips
                         tapAction:(nullable FCSRDataEmptyAction)action
                   backgroundColor:(nullable UIColor *)bgColor
                         textColor:(nullable UIColor *)textColor
                         imageSize:(CGSize)imageSize;

- (void)fcsr_removeEmptyView;

//error

- (void)fcsr_addErrorView;

- (void)fcsr_addErrorViewWithTapAction:(nullable FCSRNetworkErrorAction)action;

- (void)fcsr_addErrorView:(nullable UIImage *)image tips:(nullable NSString *)tips refreshTips:(NSString *)refreshTips;

- (void)fcsr_addErrorView:(nullable UIImage *)image
                     tips:(nullable NSString *)tips
              refreshTips:(NSString *)refreshTips
                tapAction:(nullable FCSRNetworkErrorAction)action;

- (void)fcsr_addErrorView:(nullable UIImage *)image
                     tips:(nullable NSString *)tips
              refreshTips:(NSString *)refreshTips
                tapAction:(nullable FCSRNetworkErrorAction)action
          backgroundColor:(nullable UIColor *)bgColor
                tipColor:(nullable UIColor *)tipColor
         refreshTipColor:(nullable UIColor *)refreshTipColor
                imageSize:(CGSize)imageSize;

- (void)fcsr_removeErrorView;

//给占位图添加返回按钮
- (void)fcsr_addGoBackBtn:(UIButton *_Nullable)gobackBtn
                superView:(UIView *_Nullable)superView
                   action:(void (^)(void))action;

@end

NS_ASSUME_NONNULL_END
