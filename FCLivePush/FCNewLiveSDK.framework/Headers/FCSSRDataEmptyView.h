//
//  FCSSRDataEmptyView.h
//  FCSRTools
//
//  Created by Summer on 2020/5/14.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^FCSRDataEmptyAction)(void);

@interface FCSSRDataEmptyView : UIView

- (instancetype)initWithTips:(NSString *)tips;
- (void)setBgImage:(UIImage *)image;
- (void)setbgImageSize:(CGSize)size;
- (void)setTip:(NSString *)tip;
- (void)setupEmptyTapAction:(FCSRDataEmptyAction)tapAction;
- (void)setTipTextColor:(UIColor *)textColor;


@end

NS_ASSUME_NONNULL_END
