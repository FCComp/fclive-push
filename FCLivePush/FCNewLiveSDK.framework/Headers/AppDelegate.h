//
//  AppDelegate.h
//  NewLive
//
//  Created by Summer on 2020/5/6.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FCNewLiveSDK/FCNewLiveSDK.h>


@interface AppDelegate : FCNewLiveAppDelegate <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

