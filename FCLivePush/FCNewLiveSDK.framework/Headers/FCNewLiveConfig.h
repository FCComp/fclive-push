//
//  FCNewLiveConfig.h
//  FCNewLiveSDK
//
//  Created by Summer on 2019/12/26.
//  Copyright © 2019 Summer. All rights reserved.
//

#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN

@class UIColor;
@class FCNewLiveUser;


@interface FCNewLiveConfig : NSObject

+ (FCNewLiveConfig *)instance;

/*
 主题颜色，默认wihteColor
 一般会在导航组件加载配置后根据配置设定。（可能为空，导航组件加载后才有）
 */
@property (nonatomic, strong) UIColor *themeColor;

/*
  APP背景颜色
 */
@property (nonatomic, strong) UIColor *backgroundColor;


/*域名，http://或者https://开头，+ip+端口
 一般会在导航组件加载配置后根据配置设定
 */
@property (nonatomic, copy) NSString *domain;
/**
 isOriginPopGesture = YES,采用系统默认导航栏
 isOriginPopGesture = NO,全屏侧滑导航栏
 */
@property (nonatomic, assign) BOOL isOriginPopGesture;


//-------以下为非必需


/*
  tabbar背景颜色
 */
@property (nonatomic, strong) UIColor *tabBarBackgroundColor;

/**
 导航栏tintColor颜色，即文字等颜色
 */
@property (nonatomic, strong) UIColor *navBarTextColor;

/**
 导航控制器背景颜色值类型，0：16进制色值字符串（如1479D7），1：图片base64字符串，2：图片网络链接,默认为色值字符串
 */
@property (nonatomic, assign) NSInteger navigationBarBackgroundType;

/**
 导航控制器背景颜色值,值类型参照navigationBarBackgroundType字段
 */
@property (nonatomic, strong) NSString *navigationBarBackgroundValue;

//官方domain
@property (nonatomic, copy) NSString *official_domain;
@property (nonatomic, copy) NSString *domain_name;

/*JPush推送相关key，框架使用
 */
@property (nonatomic, copy) NSString *jpushAppkey;
@property (nonatomic, copy) NSString *jpushChannel;


/*
 根据key取各个组件在“FCNewLiveConfig.plist”中自定义的配置数据
*/
 - (NSDictionary *)featureByName:(NSString *)key;

//@property (nonatomic, copy) NSDictionary *serversConfig;

@end

NS_ASSUME_NONNULL_END
