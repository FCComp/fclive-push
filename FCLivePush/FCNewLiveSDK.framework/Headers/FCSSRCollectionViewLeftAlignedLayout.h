//
//  FCSSRCollectionViewLeftAlignedLayout.h
//  FCNewLiveSDK
//
//  Created by Summer on 2020/6/24.
//  Copyright © 2020 sobey. All rights reserved.
// 靠左显示的collectionViewFlowLayout

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FCSSRCollectionViewDelegateLeftAlignedLayout<UICollectionViewDelegateFlowLayout>

@end

@interface FCSSRCollectionViewLeftAlignedLayout : UICollectionViewFlowLayout

@end

NS_ASSUME_NONNULL_END
