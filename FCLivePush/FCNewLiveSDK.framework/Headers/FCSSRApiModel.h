//
//  FCSSRApiModel.h
//  FCSRTools
//
//  Created by Summer on 2020/3/3.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FCSSRApiModel : NSObject

@property (nonatomic, assign) NSInteger code;
@property (nonatomic, strong) NSString *msg;
@property (nonatomic, strong) NSArray *data;
@end

@interface FCSSRApiObjectModel : NSObject

@property (nonatomic, assign) NSInteger code;
@property (nonatomic, strong) NSString *msg;
@property (nonatomic, strong) NSDictionary *data;
@end


NS_ASSUME_NONNULL_END
