//
//  UIDevice+FCSRDevice.h
//  FCNewLiveSDK
//
//  Created by Summer on 2020/7/1.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIDevice (FCSRDevice)

/**
 * @interfaceOrientation 输入要强制转屏的方向
 */
+ (void)fcsr_switchNewOrientation:(UIInterfaceOrientation)interfaceOrientation;

@end

NS_ASSUME_NONNULL_END
