//
//  NSString+FCSSRAttribute.h
//  FCSRTools
//
//  Created by Summer on 2020/1/14.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (FCSSRAttribute)
/********************************************************************
 *  返回包含关键字的富文本编辑
 *
 *  @param spacing 行高
 *  @param textcolor   字体颜色
 *  @param font        字体
 *  @param keyColor    关键字字体颜色
 *  @param keyFont     关键字字体
 *  @param keyWords    关键字数组
 *
 *  @return
 ********************************************************************/
- (NSMutableAttributedString *)fcsr_attributeWithLineSpacing:(CGFloat)spacing
                                       textColor:(UIColor *)textcolor
                                        textFont:(id)font
                                keyColor:(UIColor *)keyColor
                                         keyFont:(id)keyFont
                                        keyWords:(NSArray *)keyWords;

/**
 *  计算富文本字体高度
 *
 *  @param lineSpeace 行高
 *  @param font       字体
 *  @param width      字体所占宽度
 *
 *  @return 富文本高度
 */
- (CGFloat)fcsr_getAttributeHeightWithSpace:(CGFloat)lineSpeace withFont:(UIFont*)font withWidth:(CGFloat)width;

@end

NS_ASSUME_NONNULL_END
