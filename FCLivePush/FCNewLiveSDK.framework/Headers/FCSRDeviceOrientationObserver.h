//
//  FCSRDeviceOrientationObserver.h
//  FCNewLiveSDK
//
//  Created by Summer on 2020/7/2.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^FCSRDeviceOrientationObserverBlock)(UIDeviceOrientation orientation);
typedef void(^FCSRDeviceOrientationObserverDescriptionBlock)(NSString *description);

@interface FCSRDeviceOrientationObserver : NSObject

- (void)startOrientationObserverChangeWithBlock: (FCSRDeviceOrientationObserverBlock)orientationBlock;
- (void)startOrientationObserverChangeWithDescriptionBlock: (FCSRDeviceOrientationObserverDescriptionBlock)orientationBlock;

- (void)removeOrientationObserver;

@end

NS_ASSUME_NONNULL_END
