//
//  FCSRBaseVM.h
//  FCNewLiveSDK
//
//  Created by Summer on 2020/6/21.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FCSRBaseVM : NSObject
/**
 数据源
 */
@property (nonatomic, strong) NSMutableArray *dataArray;


/**
 页数
 */
@property (nonatomic, assign) NSInteger page;
/**
 每一页的个数
 */
@property (nonatomic, assign) NSInteger pageSize;

@end

NS_ASSUME_NONNULL_END
