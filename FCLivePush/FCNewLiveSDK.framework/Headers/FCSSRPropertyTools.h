//
//  FCSSRPropertyTools.h
//  FCNewLiveSDK
//
//  Created by Summer on 2020/6/19.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FCSSRPropertyTools : NSObject
//自动生成属性声明的代码
+ (void)fcsr_propertyModelWithDictionary:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
