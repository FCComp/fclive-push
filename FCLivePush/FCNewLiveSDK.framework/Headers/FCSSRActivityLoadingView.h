//
//  FCSSRActivityLoadingView.h
//  FCSRTools
//
//  Created by Summer on 2020/5/14.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FCSSRActivityLoadingView : UIView

@property (nonatomic) NSString *tips;

- (void)stopLoading;

@end

NS_ASSUME_NONNULL_END
