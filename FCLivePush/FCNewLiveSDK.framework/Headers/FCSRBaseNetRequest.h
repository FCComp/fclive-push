//
//  FCSRBaseNetRequest.h
//  FCSRTools
//
//  Created by Summer on 2020/4/12.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface FCSRBaseNetRequest : NSObject

+ (void)getWithUrl:(NSString *)url param:(NSDictionary *)param block:(void (^)(id _Nullable data,  NSString * _Nullable error))block;

+ (void)getWithUrl:(NSString *)url param:(NSDictionary *)param showError:(BOOL)showError block:(void (^)(id _Nullable data,  NSString * _Nullable error))block;

+ (void)postWithUrl:(NSString *)url param:(NSDictionary *)param block:(void (^)(id _Nullable data,  NSString * _Nullable error))block;

+ (void)postWithUrl:(NSString *)url param:(NSDictionary *)param showError:(BOOL)showError block:(void (^)(id _Nullable data,  NSString * _Nullable error))block;

+ (void)deleteWithUrl:(NSString *)url param:(NSDictionary *)param block:(void (^)(id _Nullable data,  NSString * _Nullable error))block;


+ (void)deleteWithUrl:(NSString *)url param:(NSDictionary *)param showError:(BOOL)showError block:(void (^)(id _Nullable data,  NSString * _Nullable error))block;

+ (void)putWithUrl:(NSString *)url param:(NSDictionary *)param block:(void (^)(id _Nullable data,  NSString * _Nullable error))block;

+ (void)putWithUrl:(NSString *)url param:(NSDictionary *)param showError:(BOOL)showError block:(void (^)(id _Nullable data,  NSString * _Nullable error))block;

@end

