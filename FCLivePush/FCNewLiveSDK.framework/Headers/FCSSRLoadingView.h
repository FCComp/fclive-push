//
//  FCSSRLoadingView.h
//  FCSRTools
//
//  Created by Summer on 2020/1/7.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MBProgressHUD/MBProgressHUD.h>

NS_ASSUME_NONNULL_BEGIN

@interface FCSSRLoadingView : UIView

@property(nonatomic, strong) UIActivityIndicatorView *act;
@property(nonatomic, strong) MBProgressHUD *hud;

@property(nonatomic, strong) UIView *errorView;
@property(nonatomic, strong) UIImageView *errorImageV;
@property(nonatomic, strong) UILabel *errorLbl;
@property(nonatomic, strong) UIButton *errorBtn;

@property(nonatomic, copy) void (^block)(void);


+ (FCSSRLoadingView *)share;

/**
 UIActivityIndicatorView 在当前控制器的视图上显示
 */
- (void)showActivity;

/**
UIActivityIndicatorView 在keyWindow 上显示
 */
- (void)keywindowShowActivity;

/**
 UIActivityIndicatorView 消失
 */
- (void)dismissActivity;


/**
 MBProgressHUD 在当前控制器的视图上显示
 */
- (void)showHud;

/**
MBProgressHUD 在keyWindow 上显示
 */
- (void)keywindowShowHud;

/**
 MBProgressHUD 消失
 */
- (void)dismissHud;


- (void)dismissView;


- (void)showErrorWithBlock:(void (^)(void))block;

- (void)showErrorWithImage:(NSString *)image title:(NSString *)title btnTitle:(NSString *)btnTitle block:(void (^)(void))block;




@end

NS_ASSUME_NONNULL_END
