//
//  FCNewLiveNavigationViewController.h
//  FCNewLiveNavigation
//
//  Created by Summer on 2020/1/3.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class FCNewLiveTabBarController;
@interface FCNewLiveNavigationViewController : UIViewController
@property (nonatomic, strong) UIColor *navBarTextColor;
@property (nonatomic, strong) UIViewController *firstViewController;
@property (nonatomic, strong) FCNewLiveTabBarController *tabBarController;

- (void)reConfigChildViews;
@end

NS_ASSUME_NONNULL_END
