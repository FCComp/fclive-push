//
//  FCSSRCommonTools.h
//  FCSRTools
//
//  Created by Summer on 2020/1/2.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>



@interface FCSSRCommonTools : NSObject

+ (instancetype)share;

#pragma mark UIViewController

//获取根视图
+ (UIViewController *)appRootViewController;

+ (UIViewController *)topViewController:(UIViewController *)controller;
/*! contextVC:当前的视图
    tartgetVC:寻找的视图
 */
+ (UIViewController *)contextVC:(UIViewController *)contextVC tartgetVC:(id)tartgetName;

+ (UIViewController *)getCurrentVC;

//通过xib文件 初始化对象
+ (id)getXibObjc:(NSString *)xibName;

//通过storyboard Id 初始化对象
+ (id)getVCById:(NSString *)Id;

/**
 通过Storyboard名及其ID获取控制器
 @param Id 控制器id
 @param name Storyboard名
 @return 控制器
 */
+ (id)getVCById:(NSString *)Id storyboardName:(NSString *)name;

#pragma mark UIImage


/**
 *  根据图片url获取图片尺寸
 */
+ (CGSize)getImageSizeWithURL:(id)URL;

// 压缩图片
+ (UIImage *)compressImage:(UIImage *)image toByte:(NSUInteger)maxLength;

//裁剪图片
+ (UIImage *)circleImage:(UIImage *)image size:(CGSize)size;

//改变图片的大小
+ (UIImage *)scaleToSize:(UIImage *)img size:(CGSize)size;
//改变图片的大小
+ (UIImage *)scaleToSize:(UIImage *)image ratio:(CGFloat)ratio;
// 颜色转换为背景图片
+ (UIImage *)imageWithColor:(UIColor *)color;
//重新设置图片大小
+ (NSData *)resetSizeOfImageData:(UIImage *)source_image maxSize:(NSInteger)maxSize;
//string- NSURL
+ (NSURL *)getImgUrl:(NSString *)imgStr;

//绘制虚线
+ (UIImage *)drawDottedLineWithSize:(CGSize)size lineWidth:(CGFloat)lineWidth lineColor:(UIColor *)color;
//根据size color画图返回图片
+ (UIImage *)drawColorImage:(UIColor *)color withSize:(CGSize)size;

//根据距离四周边距拉伸图片
+ (UIImage *)resizeImageWithImage:(UIImage *)image top:(CGFloat)top left:(CGFloat)left bottom:(CGFloat)bottom right:(CGFloat)right;
//默认距离四周1/3拉伸图片
+ (UIImage *)resizeImageWithImage:(UIImage *)image;
    
#pragma mark NSString

//json字符串转NSDictionary
+(NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;
//NSDictionary转json字符串
+(NSString *)dictionaryToJson:(NSDictionary *)dic;
//json字符串转NSArray
+(NSArray *)arrayWithJsonString:(NSString *)jsonString;
//NSArray转json字符串
+(NSString *)arrayToJson:(NSArray *)array;


+ (NSString *)getString:(NSString *)string;

+ (NSString *)getString:(NSString *)string withDefault:(NSString *)defaultString;

+ (NSString *)getJoinString:(NSString *)str;

//判断是否包含某个字符
+ (BOOL)judgeTheillegalCharacter:(NSString *)content;

//判断是否有emoji
+(BOOL)stringContainsEmoji:(NSString *)string;

//判断是否是纯数字
+ (BOOL)isPureNum:(NSString *)string;

//判断是否是纯字母
+ (BOOL)isPureCharacters:(NSString *)string;

// 时间戳转字符串
+ (NSString *)time_timestampToString:(NSInteger)timestamp;

// 判断银行卡号是否合法
+ (BOOL)isBankCard:(NSString *)cardNumber;
// 判断身份证号是否合法
+ (BOOL)judgeIdentityStringValid:(NSString *)identityString;

//判断手机号码格式是否正确
+ (BOOL)valiMobile:(NSString *)mobile;
//密码检查 请输入6-20包含字母与数字的组合
+ (BOOL)checkPassword:(NSString *)password;
//手机号码8-11位
+ (BOOL)checkMobile8_11:(NSString *)mobile;
//是否包含2个小数点
+ (BOOL)isContainTwoDecimals:(NSString *)decimalStr;
//是否包含小数点
+ (BOOL)isContainDecimalPoint:(NSString *)decimalStr;


+ (NSString *)trimWhitespaceAndNewline:(NSString *)val;

// 根据当前时间戳获取星期几
+ (NSString *)dateWeekWithDateString:(NSString *)dateString;

//直接传入精度丢失有问题的Double类型
+(NSString *)reviseString:(NSString *)str;

+ (double)reviseDouble:(double)number;

// 判断字符串为空
+ (BOOL)isBlankString:(NSString*)str;

/**
 获取 str 高度
 */
+ (CGFloat)getHeightWithString:(NSString *)str withFont:(UIFont *)font withWidth:(CGFloat )width;
/**
 获取 str 宽度
 */
+ (CGFloat)getWidthWithString:(NSString *)str withFont:(UIFont *)font withHeight:(CGFloat )height;

/**
 获取 htmlStr 高度
 */
+ (CGFloat)getHeightWithHtmlString:(NSString *)str withWidth:(CGFloat )width;


#pragma mark UIView

//去除searhBarBack的背景色
+ (void)removeSearhBarBack:(UISearchBar *)searchBar;

//水平方向的颜色渐变
+(void)setHorizonnalGradientColorView:(UIView *)view startColor:(UIColor *)startColor endColor:(UIColor *)endColor;

//垂直方向的颜色渐变
+(void)setVerticalGradientColorView:(UIView *)view startColor:(UIColor *)startColor endColor:(UIColor *)endColor;

+(void)setGradientColorView:(UIView *)view startColor:(UIColor *)startColor endColor:(UIColor *)endColor startPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint;

//圆角
+ (void)setCornerRadius:(CGSize)corner view:(UIView *)view;

+ (void)setCornerRadius:(CGSize)corner view:(UIView *)view leftTop:(BOOL)leftTop  rightTop:(BOOL)rightTop leftBot:(BOOL)leftBot rightBot:(BOOL)rightBot;

//设置view的边框
+(void)setViewBorderWith:(UIView *)view borderWith:(NSInteger )width withColor:(UIColor *)color;

/**
设置view阴影
0 = 顶部
1 = 底部
2 = 左侧
3 = 右侧
4 = 四周
5 = 左右底
6 = 左右上
7 = 左右
*/

+ (void)setViewShadowWith:(UIView *)view opacity:(CGFloat )opacity color:(UIColor *)color shadowPathWidth:(NSInteger )shadowPathWidth radius:(NSInteger )radius withType:(NSInteger )type;

#pragma mark Animation

+ (void)scaleAnimation:(UIView *)view;

+ (void)scaleAnimation:(UIView *)view fromValue:(CGFloat)fromValue toValue:(CGFloat) toValue;

#pragma mark other

//避免重复点击
+ (void)avoidMutiClick:(UIView *)sender;
+ (void)avoidMutiClick:(UIView *)sender sustain:(NSInteger)sustain;

//变成第一响应者
+ (void)fieldBecomfirstResbonder:(UITextField *)field;
+ (void)fieldBecomfirstResbonder:(UITextField *)field time:(float)time;

/**
 打电话
 */
+ (void)callPhone:(UIViewController *)vc withTel:(NSString *)tel;

/**
 时间戳 - 转 - 时间 yyyy年 MM月 dd日
 */
+(NSString *)getTimeWithTimestamp:(NSString *)timestamp;
/**
 时间戳 - 转 - 时间 yyyy-MM-dd HH:mm:ss
 
 */
+(NSString *)getDetailTimeWithTimestamp:(NSString *)timestamp;
/**
 时间戳 - 转 - 时间 yyyy-MM-dd
 
 */
+(NSString *)getListTimeWithTimestamp:(NSString *)timestamp;

/**
  时间 - 转 - 时间戳 yyyy年 MM月 dd日
 */
+(NSString *)getTimestampWithTime:(NSString *)time;

//去掉导航栏的横线
+(void)removeNavigationBar:(UINavigationBar *)bar;

/** 倒计时方法*/
+ (void)countDownWithTimeOut:(int)timeOut CallBack:(void (^)(BOOL isFinished, int curTime))callBack;
//
+ (void)sendNoteCodeMethodWithButton:(UIButton *)button
                                time:(CGFloat)time
                       unableBkColor:(UIColor *)unableBkColor unableImage:(UIImage *)unableImage
                         ableBkColor:(UIColor *)ableBkColor
                           ableImage:(UIImage *)ableImage;
//获取验证码
+ (void)sendNoteCodeMethodWith:(UILabel *)label withbgImgageView:(UIImageView *)bgImageView time:(CGFloat)time unableColor:(UIColor *)unableColor;

// 获取验证码
+ (void)sendNoteCodeWithLabel:(UILabel *)label unableColor:(UIColor *)unableColor ableColor:(UIColor *)ableColor;
//空数组
+ (BOOL)isBlankArray:(NSArray *)array;

+ (BOOL)isIPhoneX;

+ (CGFloat)statusBarHeight;

+ (CGFloat)navigationBarHeight;

+ (CGFloat)tabBarHeight;

+ (CGFloat)bottomSafeHeight;

+ (CGFloat)landScapeBottomSafeHeight;

+ (CGFloat)landScapeLeftSafeHeight;
    
+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message leftTitle:(NSString *)leftTitle rightTitle:(NSString *)rightTitle block:(void(^)(NSInteger index))block;

+ (UIAlertController *)showReturnAlertWithTitle:(NSString *)title message:(NSString *)message leftTitle:(NSString *)leftTitle rightTitle:(NSString *)rightTitle block:(void(^)(NSInteger index))block;

+ (void)showSheetWithTitle:(NSString *)title message:(NSString *)message titles:(NSArray *)titles block:(void(^)(NSInteger index))block;

/**
   返回货币格式
 */
+ (NSString *)getMoneyStringWithMoneyNumber:(double)money;
/**
   返回货币格式
 */
+ (NSString *)getMoneyStringWithMoney:(NSString *)money;

// 获取视频第几帧的图片
+ (UIImage *)getVideoImage:(NSURL *)videoURL atTime:(NSTimeInterval)time;

//根据颜色和透明度获取颜色
+ (UIColor *)getColor:(NSString *)pColor alpha:(CGFloat) dAlpha;

//获取APP名称
+ (NSString *)getAppName;
//获取APP版本号
+ (NSString *)getAppVersion;
 
/**
 * 获取文件夹尺寸
 * @param directoryPath 文件夹路径
 * @param complete 计算完毕的Block回调
 */

+ (void)getCacheSize:(NSString *)directoryPath complete:(void(^)(NSString *total))complete;

/**
 * 清空缓存
 * @param directoryPath 文件夹路径
 */
+ (void)removeCache:(NSString *)directoryPath complete:(void(^)(BOOL success))complete;

@end


