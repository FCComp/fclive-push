//
//  FCSSRApiListOutModel.h
//  FCSRTools
//
//  Created by Summer on 2020/3/3.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FCSSRApiListInerModel : NSObject

@property (nonatomic , assign) NSInteger  total;//列表总数
@property (nonatomic , assign) NSInteger  per_page;//每页的条数
@property (nonatomic , assign) NSInteger  current_page;//当前页
@property (nonatomic , assign) NSInteger last_page;//上一页
@property (nonatomic , assign) NSInteger total_page;//总共页数
@property (nonatomic , strong) NSArray  *data;
@property (nonatomic , copy) NSString *point;//我的积分总数
@property (nonatomic , copy) NSString *total_point;//总积分
@property (nonatomic , assign) NSInteger  task_count;//任务数
@property (nonatomic , assign) NSInteger  satisfy_degree;//满意度

@end

@interface FCSSRApiListOutModel : NSObject

@property (nonatomic , strong) FCSSRApiListInerModel  *data;
@property (nonatomic , assign) NSInteger  code;
@property (nonatomic , assign) NSInteger  status;
@property (nonatomic , copy) NSString  *msg;

@end

NS_ASSUME_NONNULL_END
