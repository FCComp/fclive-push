//
//  UIView+FCSSRViewChainAble.h
//  FCSRTools
//
//  Created by Summer on 2020/5/14.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^MasonryConfig)(MASConstraintMaker *make);
typedef void (^ViewConfig)(void);

@interface UIView (FCSSRViewChainAble)

@property (nonatomic) MasonryConfig afterAttachLayout;
@property (nonatomic) ViewConfig beforeAttachConfig;
@property (nonatomic) ViewConfig afterAttachConfig;
@property (nonatomic) ViewConfig viewConfig;


/// 对view进行配置操作, 返回self
- (instancetype)config:(void (^)(__kindof UIView *view))configBlock;

/// 对view添加到superView, 返回self
- (instancetype)addToSuperView:(UIView *)theSuperView;

/// 对view使用Masonry, 返回self
- (instancetype)layout:(MasonryConfig)makeBlock;

@end

NS_ASSUME_NONNULL_END
