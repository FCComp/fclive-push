//
//  FCNewLiveUser.h
//  FCNewLiveSDK
//
//  Created by Summer on 2020/6/17.
//  Copyright © 2020 sobey. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

//用户信息
@interface FCNewLiveUserInfo : NSObject
@property (nonatomic , assign) NSInteger id;//用户ID
@property (nonatomic , copy) NSString *name;//用户名字
@property (nonatomic , copy) NSString *user_portrait;//用户头像
//非必需
@property (nonatomic , copy) NSString *real_name;
@property (nonatomic , copy) NSString *mobile;
@property (nonatomic , copy) NSString *email;
@property (nonatomic , copy) NSString *ip;
@property (nonatomic , assign) NSInteger status;
@property (nonatomic , assign) NSInteger type;
@property (nonatomic , assign) NSInteger department_id;
@property (nonatomic , assign) NSInteger create_user_id;
@property (nonatomic , assign) NSInteger tenant_id;
@property (nonatomic , strong) NSArray *role_ids;
@property (nonatomic , copy) NSString *create_time;
@property (nonatomic , copy) NSString *update_time;
@property (nonatomic , copy) NSString *status_name;
@property (nonatomic , assign) NSInteger matrix_id;
@end

//租户信息
@interface FCNewLiveTenantInfo : NSObject

@property (nonatomic , assign) NSInteger id;//租户ID
@property (nonatomic , copy) NSString *name;//租户名字
@property (nonatomic , copy) NSString *access_key;//租户access_key
//非必需
@property (nonatomic , assign) NSInteger status;
@property (nonatomic , copy) NSString *source;
@property (nonatomic , copy) NSString *create_time;
@property (nonatomic , copy) NSString *update_time;

@end

@interface FCNewLiveUserServiceAccess : NSObject

@property (nonatomic , strong) NSArray *fslive;
@end



@interface FCNewLiveUser : NSObject<NSCoding,NSCopying>

@property (nonatomic, copy) NSString *token;//登陆后的token
@property (nonatomic , strong) FCNewLiveUserInfo *userInfo;
@property (nonatomic , strong) FCNewLiveTenantInfo *tenantInfo;
@property (nonatomic , strong) FCNewLiveUserServiceAccess *service_access;

@property (nonatomic , assign) NSInteger portraitType;//portraitType = 0,没有竖屏权限，1 = 有竖屏权限

//非必需
/**
 domain_type = 0,官方站点
 domain_type = 1,私有化站点
 */
@property (nonatomic, assign) NSInteger domain_type;
@property (nonatomic, copy) NSString *custom_domain;


+ (FCNewLiveUser *)instance;

//删除本地用户信息
+ (void)terminateInstance;

//将用户信息写入本地
+ (void)persist;

//设置user
- (void)setUserInfoDict:(NSDictionary *)user;





@end

NS_ASSUME_NONNULL_END
